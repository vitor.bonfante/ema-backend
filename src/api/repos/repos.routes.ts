import { Router } from "express";
import { searchReposByUser } from "./controllers/repos.searchReposByUser";

const reposRouter = Router();

reposRouter.get("/search", searchReposByUser);

export { reposRouter };
