import axios from "axios";

function findUserRepos(userName: string) {
  return axios.get(`https://api.github.com/users/${userName}/repos`);
}

export { findUserRepos };
