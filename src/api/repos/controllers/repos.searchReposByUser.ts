import { NextFunction, Request, Response } from "express";
import { CustomError } from "../../../models/customError";
import { findUserRepos } from "../services/repos.findUserRepos";

interface ReturnProps {
  lastCreated: [
    {
      name: string;
      url: string;
    }
  ];
  bestRated: [
    {
      name: string;
      url: string;
    }
  ];
}

interface ApiReturn {
  created_at: string;
  stargazers_count: number;
  name: string;
  html_url: string;
}

async function searchReposByUser(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { userName } = req.query;

    if (!userName) {
      res.status(400);
      throw new CustomError(
        "UserName inválido",
        400,
        "Insira um nome de usuário para pesquisar"
      );
    }

    var apiReturn: ApiReturn[] = [];
    await findUserRepos(userName as string).then((res) => {
      apiReturn = res.data;
    });

    //ordena por data
    apiReturn.sort(function (a, b) {
      return (
        new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
      );
    });

    var lastCreated = [];

    for (let i = 0; i < 4; i++) {
      if (apiReturn[i]) {
        lastCreated.push({
          name: apiReturn[i].name,
          url: apiReturn[i].html_url,
        });
      }
    }

    //ordena por stars
    apiReturn.sort(function (a, b) {
      return b.stargazers_count - a.stargazers_count;
    });

    var bestRated = [];

    for (let i = 0; i < 4; i++) {
      if (apiReturn[i]) {
        bestRated.push({
          name: apiReturn[i].name,
          url: apiReturn[i].html_url,
        });
      }
    }

    const repos = {
      bestRated,
      lastCreated,
    };

    return res.status(200).json(repos);
  } catch (error) {
    console.log(error);
    next(error);
  }
}

export { searchReposByUser };
