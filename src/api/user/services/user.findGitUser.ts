import axios from "axios";

function findGitUser(userName: string, page: string) {
  if (!page) page = "1";
  return axios.get(
    `https://api.github.com/search/users?q=${userName}&per_page=20&page=${page}`
  );
}

export { findGitUser };
